FROM multiarch/ubuntu-core:armhf-xenial
ARG ARCH=armhf

RUN apt-get update && apt-get -y upgrade && apt-get -y autoremove && apt-get -y clean && apt-get -f -y install build-essential wget pkg-config curl sudo unattended-upgrades
RUN curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash - && apt-get -y install nodejs node-gyp git
